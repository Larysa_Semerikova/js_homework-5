function createNewUser(){

    let name = prompt("What is your name?");
    let surname = prompt ("What is your surname?")
    let birthDate = prompt ("Enter the date of your birth", "dd.mm.yyyy")

    newUser ={
        firstName: name,
        lastName: surname,
        birthday: birthDate,

        getLogin(){
            return `${newUser.firstName[0]}`.toLowerCase() + `${newUser.lastName}`.toLowerCase();

        },

        getAge(){
            let dd = newUser.birthday.substring(0, 2);
            let mm = newUser.birthday.substring(3, 5);
            let yyyy = newUser.birthday.substring(6);
            let isBirthday = new Date(yyyy, mm, dd);
            const today = new Date()
            return Math.floor((Date.parse(today) - Date.parse(isBirthday))/60/60/24/365/1000)
        },
        
        getPassword(){
            return `${newUser.firstName[0]}`.toUpperCase() + `${newUser.lastName}`.toLowerCase() + `${newUser.birthday}`.slice(6);
        }
    }  
  
}


// createNewUser();
console.log(createNewUser());
console.log(newUser.getAge())
console.log(newUser.getPassword())



